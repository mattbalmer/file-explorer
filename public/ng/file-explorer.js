angular.module('file-explorer', ['templates.file-explorer', 'ngTouch', 'ngRoute', 'angularFileUpload']);
angular.module('file-explorer').controller('FileDetailsController', function($scope, $routeParams, utils) {
    $scope.$on('showDetails', function(event, file) {
        console.log('showDetails');
        $scope.active = true;
        $scope.file = file;
        $scope.hasPreview = utils.hasPreview(file);
    });

    $scope.close = function() {
        $scope.active = false;
        $scope.file = {};
        $scope.hasPreview = false;
    };

    $scope.path = function(file) {
        return utils.join($routeParams.path || '', file.name);
    };
});
angular.module('file-explorer').directive('fileDetails', function() {
    return {
        restrict: 'E',
        templateUrl: '/html/file-explorer/components/file-details/template.html',
        controller: 'FileDetailsController',
        scope: true
    }
});
angular.module('file-explorer').controller('HeaderController', function($rootScope, $scope, $location, $upload, utils) {
    $scope.go = function($index) {
        var path = $scope.dirs.slice(0, $index + 1);
        path = utils.join.apply(null, path);
        $location.path(path);
    };

    $scope.upload = function() {
        $rootScope.$broadcast('upload');
    };
});
angular.module('file-explorer').directive('header', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/html/file-explorer/components/header/template.html',
        scope: {
            path: '='
        },
        link: function($scope) {
            $scope.dirs = ['/'].concat($scope.path ? $scope.path.split('/') : []);
        },
        controller: 'HeaderController'
    }
});
angular.module('file-explorer').controller('ThingController', function($rootScope, $scope, $location, $routeParams, utils) {
    $scope.go = function(thing) {
        var path = utils.join($routeParams.path || '', thing.name);
        $location.path(path);
    };

    $scope.details = function(thing) {
        $rootScope.$broadcast('showDetails', thing);
    }
});
angular.module('file-explorer').directive('thing', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/html/file-explorer/components/thing/template.html',
        scope: {
            thing: '='
        },
        controller: 'ThingController'
    }
});
angular.module('file-explorer').controller('UploadOverlayController', function($scope, $routeParams, $window, $upload) {
    $scope.models = {};

    $scope.$on('upload', function() {
        console.log('upload');
        $scope.active = true;
    });

    $scope.close = function() {
        $scope.active = false;
    };

    $scope.submit = function(form) {
        console.log('models', form);

        $upload.upload({
            url: '/api/upload/' + ($routeParams.path || ''),
            fields: form,
            file: form.files
        })
            .finally(function() {
                $window.location.reload();
            });
    }
});
angular.module('file-explorer').directive('uploadOverlay', function() {
    return {
        restrict: 'E',
        templateUrl: '/html/file-explorer/components/upload-overlay/template.html',
        controller: 'UploadOverlayController',
        scope: true
    }
});
/* https://gist.github.com/thomseddon/3511330 */
angular.module('file-explorer').filter('bytes', function() {
    return function(bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
        if (typeof precision === 'undefined') precision = 1;
        var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
            number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    }
});
angular.module('file-explorer').config(function($locationProvider, $routeProvider) {
    $routeProvider
        .when('/:path*?', {
            controller: 'Main',
            templateUrl: '/html/file-explorer/views/main/template.html'
        })
        .otherwise({ redirectTo: '/' });
});
angular.module('file-explorer').service('api', function($http) {
    var api = {};

    api.list = function(dir) {
        return $http.get('/api/list/' + (dir || ''));
    };

    return api;
});
angular.module('file-explorer').service('utils', function() {
    var utils = {};

    utils.join = function(paths) {
        var paths = Array.prototype.slice.call(arguments, 0);

        return paths.join('/')
            .replace(/\\/g, '/')
            .replace(/(\/\/+)/g, '/');
    };

    utils.hasPreview = function(file) {
        if(!file.type) return false;
        return file.type.mime.indexOf('image') > -1;
    };

    return utils;
});
angular.module('file-explorer').controller('Main', function($scope, $routeParams, api) {
    $scope.path = $routeParams.path;

    api.list($routeParams.path)
        .then(function(res) {
            $scope.things = res.data;
            console.log($scope.things);
        })
});