angular.module('file-explorer').config(function($locationProvider, $routeProvider) {
    $routeProvider
        .when('/:path*?', {
            controller: 'Main',
            templateUrl: '/html/file-explorer/views/main/template.html'
        })
        .otherwise({ redirectTo: '/' });
});