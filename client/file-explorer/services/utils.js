angular.module('file-explorer').service('utils', function() {
    var utils = {};

    utils.join = function(paths) {
        var paths = Array.prototype.slice.call(arguments, 0);

        return paths.join('/')
            .replace(/\\/g, '/')
            .replace(/(\/\/+)/g, '/');
    };

    utils.hasPreview = function(file) {
        if(!file.type) return false;
        return file.type.mime.indexOf('image') > -1;
    };

    return utils;
});