angular.module('file-explorer').service('api', function($http) {
    var api = {};

    api.list = function(dir) {
        return $http.get('/api/list/' + (dir || ''));
    };

    return api;
});