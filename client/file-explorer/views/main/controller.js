angular.module('file-explorer').controller('Main', function($scope, $routeParams, api) {
    $scope.path = $routeParams.path;

    api.list($routeParams.path)
        .then(function(res) {
            $scope.things = res.data;
            console.log($scope.things);
        })
});