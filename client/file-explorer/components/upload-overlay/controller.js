angular.module('file-explorer').controller('UploadOverlayController', function($scope, $routeParams, $window, $upload) {
    $scope.models = {};

    $scope.$on('upload', function() {
        console.log('upload');
        $scope.active = true;
    });

    $scope.close = function() {
        $scope.active = false;
    };

    $scope.submit = function(form) {
        console.log('models', form);

        $upload.upload({
            url: '/api/upload/' + ($routeParams.path || ''),
            fields: form,
            file: form.files
        })
            .finally(function() {
                $window.location.reload();
            });
    }
});