angular.module('file-explorer').directive('uploadOverlay', function() {
    return {
        restrict: 'E',
        templateUrl: '/html/file-explorer/components/upload-overlay/template.html',
        controller: 'UploadOverlayController',
        scope: true
    }
});