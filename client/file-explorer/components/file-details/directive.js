angular.module('file-explorer').directive('fileDetails', function() {
    return {
        restrict: 'E',
        templateUrl: '/html/file-explorer/components/file-details/template.html',
        controller: 'FileDetailsController',
        scope: true
    }
});