angular.module('file-explorer').controller('FileDetailsController', function($scope, $routeParams, utils) {
    $scope.$on('showDetails', function(event, file) {
        console.log('showDetails');
        $scope.active = true;
        $scope.file = file;
        $scope.hasPreview = utils.hasPreview(file);
    });

    $scope.close = function() {
        $scope.active = false;
        $scope.file = {};
        $scope.hasPreview = false;
    };

    $scope.path = function(file) {
        return utils.join($routeParams.path || '', file.name);
    };
});