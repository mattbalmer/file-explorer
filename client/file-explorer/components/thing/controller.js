angular.module('file-explorer').controller('ThingController', function($rootScope, $scope, $location, $routeParams, utils) {
    $scope.go = function(thing) {
        var path = utils.join($routeParams.path || '', thing.name);
        $location.path(path);
    };

    $scope.details = function(thing) {
        $rootScope.$broadcast('showDetails', thing);
    }
});