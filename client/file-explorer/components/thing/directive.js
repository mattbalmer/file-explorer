angular.module('file-explorer').directive('thing', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/html/file-explorer/components/thing/template.html',
        scope: {
            thing: '='
        },
        controller: 'ThingController'
    }
});