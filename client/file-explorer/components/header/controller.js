angular.module('file-explorer').controller('HeaderController', function($rootScope, $scope, $location, $upload, utils) {
    $scope.go = function($index) {
        var path = $scope.dirs.slice(0, $index + 1);
        path = utils.join.apply(null, path);
        $location.path(path);
    };

    $scope.upload = function() {
        $rootScope.$broadcast('upload');
    };
});