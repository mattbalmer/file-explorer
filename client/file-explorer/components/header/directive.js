angular.module('file-explorer').directive('header', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/html/file-explorer/components/header/template.html',
        scope: {
            path: '='
        },
        link: function($scope) {
            $scope.dirs = ['/'].concat($scope.path ? $scope.path.split('/') : []);
        },
        controller: 'HeaderController'
    }
});