module.exports = function (grunt) {
    grunt.initConfig({
        stylus: {
            ng_modules: {
                options: {
                    debug: true,
                    compress: false,
                    paths: [ 'client', 'lib/stylus' ],
                    import: [
                        'nib'
                    ]
                },
                files: [ {
                    cwd: 'client',
                    src: '**/*.styl',
                    dest: 'client',
                    expand: true,
                    ext: '.css'
                } ]
            }
        },
        jade: {
            ng_modules: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [ {
                    cwd: 'client',
                    src: '**/*.jade',
                    dest: 'client',
                    expand: true,
                    ext: '.html'
                } ]
            },
            public: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [ {
                    cwd: 'public',
                    src: '**/*.jade',
                    dest: 'public',
                    expand: true,
                    ext: '.html'
                } ]
            }
        },
        ng_modules: {
            options: {
                minify: false,
                cacheViews: true
            },
            local: {
                src: 'client',
                dest: 'public/ng'
            }
        },
        watch: {
            files: [ 'client/**/*', '!client/**/*.css', '!client/**/*.html', 'public/**/*.jade'  ],
            tasks: [ 'compile' ]
        },
        clean: {
            ng_modules: [ 'client/**/*.css', 'client/**/*.html' ]
        }
    });

    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-ng-modules');

    var compileTasks = ['stylus', 'jade', 'ng_modules', 'clean'];
    grunt.registerTask('compile', compileTasks);
    grunt.registerTask('default', compileTasks.concat(['watch']));
};