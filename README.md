file-explorer
=====

Browser based file explorer

Install
-----

NodeJS is required to run this project.

    git clone https://bitbucket.org/mattbalmer/file-explorer.git

Move to the project directory

    npm install

    bower install

Run (Windows)
-----

Make sure you're in the project directory.

Set `FILE_DIR` equal to whatever directory you want this project to explore.

    set NODE_PATH=server/lib/

    set FILE_DIR=C:/Path/To/Root

    node server/server.js

Run (Mac/Linux)
-----

Make sure you're in the project directory.

Set `FILE_DIR` equal to whatever directory you want this project to explore.

    export FILE_DIR=/path/to/root

    npm start
