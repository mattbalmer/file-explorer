var express = require('express'),
    explorer = require('explorer'),
    bodyParser = require('body-parser'),
    multiparty = require('connect-multiparty'),
    routes = module.exports = express.Router();

routes.get('/list/*', function(req, res) {
    var dir = req.params[0];

    var contents = explorer.list(dir);

    res.json(contents);
});

routes.get('/download/*', function(req, res) {
    var filepath = req.params[0];

    if(explorer.exists(filepath)) {
        res.download(explorer.pathTo(filepath));
    } else {
        res.status(404).send(null);
    }
});

routes.get('/raw/*', function(req, res) {
    var filepath = req.params[0];

    if(explorer.exists(filepath)) {
        res.sendfile(explorer.pathTo(filepath));
    } else {
        res.status(404).send(null);
    }
});

routes.post('/upload/*', bodyParser.urlencoded(), multiparty(), function(req, res) {
    explorer.save(req.files, req.params[0]);
    res.status(201).send(null);
});