var path = require('path'),
    fs = require('fs'),
    config = require('config'),
    readChunk = require('read-chunk'),
    fileType = require('file-type'),
    explorer = module.exports = {};

// If relative, start from project base, if absolute - just use home_dir
var BASE_PATH = config.home_dir[0] == '/' || config.home_dir[1] == ':' ? config.home_dir
    : path.join(__dirname, '../..', config.home_dir);

explorer.list = function(dir) {
    var base = path.join(BASE_PATH, dir);
    return fs.readdirSync(base)
        .map(function(filename) {
            try {
                var stats = fs.lstatSync(path.join(base, filename));

                var type = stats.isFile() ? fileType(readChunk.sync(path.join(base, filename), 0, 262)) : undefined;

                return {
                    name: filename,
                    isFile: stats.isFile(),
                    isDir: stats.isDirectory(),
                    type: type,
                    size: stats.size,
                    created_at: stats.birthtime || stats.ctime,
                    modified_at: stats.mtime
                }
            } catch(e) {
                return {
                    name: filename
                }
            }
        });
};

explorer.exists = function(filepath) {
    return fs.existsSync( path.join(BASE_PATH, filepath) );
};

explorer.pathTo = function(filepath) {
    return path.join(BASE_PATH, filepath);
};

explorer.save = function(files, savePath) {
    for(var k in files) {
        if(!files.hasOwnProperty(k)) continue;
        var file = files[k];

        var newPath = path.join(BASE_PATH, savePath, file.name),
            is = fs.createReadStream(file.path),
            os = fs.createWriteStream(newPath);

        is.pipe(os);
        is.on('end',function() {
            fs.unlinkSync(file.path);
        });
    }
};