module.exports = {
    local: {
        env: 'development',
        port: 3000,
        home_dir: process.env.FILE_DIR || 'E:/'
    },
    heroku: {
        env: 'heroku',
        port: process.env.PORT,
        home_dir: process.env.FILE_DIR || 'files'
    }
}[process.env.NODE_ENV || 'local'];
