var express = require('express'),
    fs = require('fs'),
    path = require('path'),
    config = require('config');

var app = express();

app.use('/api', require('./api'));

app.use( express.static( path.join(__dirname, '../public')) );

app.listen(config.port, function() {
    console.log('Server now listening in %s mode on port %s', config.env, config.port);
});